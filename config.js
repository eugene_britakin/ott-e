'use strict';

/**
 * Конфиг
 * @return {Function}
 */
module.exports = function () {
    return {
        redis: {
            host: 'localhost',
            port: 6379
        }
    };
};