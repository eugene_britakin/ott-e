'use strict';

var argv = require('minimist')(process.argv.slice(2));
var log4js = require('log4js');
var config = require('./config')();
var Application = require('./src/Application');

config.app = 'WORKER::' + process.pid;
var logger = log4js.getLogger(config.app);

if (process.env.NODE_ENV === 'production') {
    logger.setLevel('ERROR');
}

process.on('uncaughtException', function (error) {
    logger.error(error);
    process.exit();
});

var app = new Application(config, logger);

if (argv.getErrors) {
    var limit = parseInt(argv.getErrors);
    if (!limit || isNaN(limit) || limit > 100) {
        limit = 50;
    }

    app.getErrors(limit, function () {
        process.exit();
    });
} else {
    app.run();
}

