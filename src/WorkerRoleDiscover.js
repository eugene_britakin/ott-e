'use strict';

var util = require('util');
var EventEmitter = require('events');

/**
 * Отвечает за определение роли воркера (генератор или обработчик)
 *
 * @param {WorkerRoleDiscoverModel} workerRoleDiscoverModel
 * @param {String} workerName
 * @param {Object} logger
 * @constructor
 */
var WorkerRoleDiscover = function (workerRoleDiscoverModel, workerName, logger) {

    /**
     * Название воркера, используется для записи последнего герератора
     * @type {String}
     * @private
     */
    this._workerName = workerName;

    /**
     * Модель для сохранения состояния активного генератора
     * @type {WorkerRoleDiscoverModel}
     * @private
     */
    this._workerRoleDiscoverModel = workerRoleDiscoverModel;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;

    /**
     * Время тайм-аута выбора генератора, секунды
     * @type {Number}
     * @private
     */
    this._chooseGeneratorTimeoutTime = 2;


    /**
     * Время тайм-аута сообщения, что генератор еще "жив", секунды
     * @type {Number}
     * @private
     */
    this._aliveGeneratorTimeoutTime = 2;

    /**
     * Время хранения состояния сколько "жив" генератор
     * @type {Number}
     * @private
     */
    this._expireProlongTime = 5;

    /**
     * Служебное поле, используется для очистки тайм-аута попытки выброра генератора
     * @type {Function}
     * @private
     */
    this._timeoutChooseGenerator = null;

    /**
     * Служебное поле, используется для очистки тайм-аута, когда воркер говорит, что он генератор и еще "жив"
     * @type {Function}
     * @private
     */
    this._timeoutAliveGenerator = null;
};

util.inherits(WorkerRoleDiscover, EventEmitter);

/**
 * Старт обнаружения/определение роли воркера приложения
 */
WorkerRoleDiscover.prototype.discover = function () {
    this._logger.debug('Start discover');
    this._tryChooseGenerator(true);
};

/**
 * Пытается выбрать роль генератора
 * @param {Boolean} firstDetect первичное обнаружение генератора
 * @private
 */
WorkerRoleDiscover.prototype._tryChooseGenerator = function (firstDetect) {

    this._workerRoleDiscoverModel.tryChooseGenerator(
        this._workerName,
        function (error, result) {
            if (error) {
                this._logger.error(error);
                return;
            }

            if (result === true) {
                this._logger.warn('!!! CHOOSEN NEW GENERATOR !!!');
                this.emit('is_generator_worker');
                this._prolongChoosenGenerator();

                return;
            }

            if (firstDetect) {
                this.emit('is_handler_worker');
            }

            this._delayTryChooseGenerator();

        }.bind(this)
    );
};

/**
 * Выполняет продление состояния, что воркер "живой" генератор
 * @private
 */
WorkerRoleDiscover.prototype._prolongChoosenGenerator = function () {

    this._workerRoleDiscoverModel.prolongChoosenGenerator(
        this._workerName,
        this._expireProlongTime,
        function (error, result) {
            if (error) {
                this._logger.error(error);
                return;
            }

            this._delayProlongChoosenGenerator();

        }.bind(this)
    );
};

/**
 * Отложенная попытка выбрать генератор
 * @private
 */
WorkerRoleDiscover.prototype._delayTryChooseGenerator = function () {

    if (this._timeoutChooseGenerator) {
        clearTimeout(this._timeoutChooseGenerator);
    }

    this._timeoutChooseGenerator = setTimeout(
        this._tryChooseGenerator.bind(this, false),
        this._chooseGeneratorTimeoutTime * 1000
    );
};

/**
 * Отложенное продление роли воркера генератора
 * @private
 */
WorkerRoleDiscover.prototype._delayProlongChoosenGenerator = function () {

    if (this._timeoutAliveGenerator) {
        clearTimeout(this._timeoutAliveGenerator);
    }

    this._timeoutAliveGenerator = setTimeout(
        this._prolongChoosenGenerator.bind(this),
        this._aliveGeneratorTimeoutTime * 1000
    );
};

module.exports = WorkerRoleDiscover;
