'use strict';

var ErrorsMessagesReportModel = require('./Model/ErrorsMessagesReportModel');

/**
 * Формирует и выводит на экран отчет сообщений обработтыне ранее с ошибкой
 *
 * @param {Object} redisClient
 * @param {Object} logger
 * @constructor
 */
var ErrorsMessagesReporter = function (redisClient, logger) {

    /**
     * Клиент для работы с redis
     * @type {Object}
     * @private
     */
    this._redisClient = redisClient;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;
};

/**
 * Старт вывода отчета
 * @param {Number} limit ограничение получения заисей на одну итерацию, значение от 1 до 100
 * @param {Function} done
 */
ErrorsMessagesReporter.prototype.output = function (limit, done) {

    var errorsMessagesModel = new ErrorsMessagesReportModel(this._redisClient, this._logger);
    limit = parseInt(limit);
    if (!limit || isNaN(limit) || limit > 100) {
        throw new Error('Wrong limit error messages, 1-100');
    }

    // выдавливаем сообщения для отображения
    errorsMessagesModel.popMessages(
        limit,
        this._printMessages.bind(this),
        function (report) {

            console.dir(report);

            done();
        }
    );
};

/**
 * Вывод сообщений ...
 * @param {Array} messages
 * @private
 */
ErrorsMessagesReporter.prototype._printMessages = function (messages) {
    // some other logic ;)
    for (var i = 0, total = messages.length; i < total; i++) {
        console.log('MESSAGE', messages[i]);
    }
};

module.exports = ErrorsMessagesReporter;
