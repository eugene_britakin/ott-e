'use strict';

var redis = require('redis');

var WorkerRoleDiscover = require('./WorkerRoleDiscover');
var WorkersManager = require('./WorkersManager');
var ErrorsMessagesReporter = require('./ErrorsMessagesReporter');

var MessagesModel = require('./Model/MessagesModel');
var WorkerRoleDiscoverModel = require('./Model/WorkerRoleDiscoverModel');

/**
 * Приложение
 *
 * @param {Object} config
 * @param {Object} logger
 * @constructor
 */
var Application = function (config, logger) {

    /**
     * Конфиг приложения
     * @type {Object}
     * @private
     */
    this._config = config;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;
};

/**
 * Запуск логики работы воркеров
 */
Application.prototype.run = function () {
    this._logger.info('Start application');

    var redisClient = redis.createClient(this._config.redis);
    var subscriberRedisClient = redis.createClient(this._config.redis);

    var messagesModel = new MessagesModel(
        redisClient,
        subscriberRedisClient,
        this._logger
    );

    var workersManager = new WorkersManager(
        messagesModel,
        this._logger
    );

    var workerRoleDiscoverModel = new WorkerRoleDiscoverModel(
        redisClient,
        this._logger
    );

    var workerRoleDiscover = new WorkerRoleDiscover(
        workerRoleDiscoverModel,
        this._config.app,
        this._logger
    );

    workerRoleDiscover.on('is_generator_worker', function () {
        workersManager.setGeneratorRole();
    }.bind(this));

    workerRoleDiscover.on('is_handler_worker', function () {
        workersManager.setHandlerRole();
    }.bind(this));

    workerRoleDiscover.discover();
};

/**
 * Получение и вывод сообщений обработанных с ошибкой
 * @param {Number} limit
 * @param {Function} done
 */
Application.prototype.getErrors = function (limit, done) {

    var redisClient = redis.createClient(this._config.redis);
    var errorsMessagesReporter = new ErrorsMessagesReporter(
        redisClient,
        this._logger
    );

    errorsMessagesReporter.output(limit, done);
};

module.exports = Application;
