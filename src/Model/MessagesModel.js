'use strict';

/**
 * Модель сообщений, сохранение/обновление, уведомеление о новых сообщениях
 *
 * @param {Object} redisClient
 * @param {Object} subscriberRedisClient
 * @param {Object} logger
 * @constructor
 */
var MessagesModel = function (redisClient, subscriberRedisClient, logger) {

    this._logger = logger;

    this._redisClient = redisClient;

    this._subscriberRedisClient = subscriberRedisClient;

    this._newMessagesNamePrefix = 'new:';

    this._messagesChannelName = 'mega_messages_channel';
};

MessagesModel.prototype.subscribe = function (done) {

    this._subscriberRedisClient.on('message', function (channel, keyName) {

        this._handleNewMessage(keyName, done);

    }.bind(this));

    this._subscriberRedisClient.subscribe(this._messagesChannelName);
};

MessagesModel.prototype.unsubscribe = function () {
    this._subscriberRedisClient.unsubscribe();
    this._subscriberRedisClient.quit();
};

MessagesModel.prototype.publishNewMessage = function (msg, done) {

    var key = this._newMessagesNamePrefix + 'message:' + msg;

    this._redisClient.multi()
        .set(key, msg)
        .publish(this._messagesChannelName, key)
        .exec(function (error, replies) {
            if (error) {
                done(error);
                return;
            }

             done(null, replies);

        }.bind(this));
};

MessagesModel.prototype.saveErrorMessage = function (name, msg, done) {

    this._redisClient.set('error:' + name, msg, done);

};

MessagesModel.prototype.saveHandlendMessage = function (name, msg, done) {

    this._redisClient.set('done:' + name, msg, done);
};

MessagesModel.prototype._handleNewMessage = function (keyName, done) {

    this._redisClient.multi()
        .get(keyName)
        .del(keyName)
        .exec(function (error, replies) {
            if (error) {
                done(error);
                return;
            }

            if (!replies.length || replies[0] === null) {
                return;
            }

            keyName = keyName.replace(this._newMessagesNamePrefix, '');

            done(null, keyName, replies[0]);

        }.bind(this));
};


module.exports = MessagesModel;
