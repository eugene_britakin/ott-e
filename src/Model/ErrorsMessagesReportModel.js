'use strict';

var waterfall = require('async/waterfall');

/**
 * Модель получение и удаление сообщений обработанных с ошибкой, для формирования отчета
 *
 * @param {Object} redisClient
 * @param {Object} logger
 * @constructor
 */
var ErrorsMessagesReportModel = function (redisClient, logger) {

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;

    /**
     * Клиент redis
     * @type {Object}
     * @private
     */
    this._redisClient = redisClient;

    /**
     * Отчет работы
     * @type {{totalErrors: number, deletedErrors: number}}
     * @private
     */
    this._report = {
        totalErrors: 0,
        deletedErrors: 0
    };

    /**
     * Лимит на скан, в одной итерации
     * @type {Number}
     * @private
     */
    this._scanLimit = 50;

    /**
     * Вызывается на каждой итерации полученных сообщений
     * @type {Function}
     * @private
     */
    this._iterationCallback = null;
};

/**
 * Выдавливает сообщения, получает и удаляет
 * @param {Number} scanLimit
 * @param {Function} iterationCallback вызвается на каждой итерации получения сообщений
 * @param {Function} complete вызывается при завершение получения всех сообщений
 */
ErrorsMessagesReportModel.prototype.popMessages = function (scanLimit, iterationCallback, complete) {

    this._scanLimit = scanLimit;

    this._iterationCallback = iterationCallback;

    this._go(0, complete);
};

/**
 * Запуск
 *
 * @param {Number} cursor
 * @param {Function} complete
 * @private
 */
ErrorsMessagesReportModel.prototype._go = function (cursor, complete) {
    // Где же вы Промисы :(
    waterfall([

        //////////////////////////////////////////////////

        function (done) {

            // получаем сообщения
            this._scanErrorsMessages(cursor, done);

        }.bind(this),

        //////////////////////////////////////////////////

        function (nextCursor, keysNames, done) {

            if (!keysNames || !keysNames.length) {
                // если нет сообщений с ошибками выходим
                done(true);
                return;
            }

            // получаем значения самих сообщений
            this._getErrorsMessages(nextCursor, keysNames, done);

        }.bind(this),

        //////////////////////////////////////////////////

        function (nextCursor, keysNames, done) {

            // удаляем показанные сообщения
            this._removeAllErrorsMessages(nextCursor, keysNames, done);

        }.bind(this)

        //////////////////////////////////////////////////

    ], function (error, nextCursor) {

        // если есть ошибка и не выход из waterfall
        if (error && error !== true) {
            this._logger.error(error);
            return;
        }

        if (nextCursor) {
            this._go(nextCursor, complete);
            return;
        }

        complete(this._report);

    }.bind(this))
};


/**
 * Получение имен ключей
 * @param {Number} cursor
 * @param {Function} done
 * @private
 */
ErrorsMessagesReportModel.prototype._scanErrorsMessages = function (cursor, done) {

    this._redisClient.scan(
        [cursor, 'MATCH', 'error:*', 'COUNT', this._scanLimit],
        function (error, result) {
            if (error) {
                done(error);
                return;
            }

            if (!result[0] || typeof result[0] !== 'string') {
                done(new Error('Invalid cursor number ' + typeof result[0]));
                return;
            }

            if (!result[1] || !Array.isArray(result[1])) {
                done(new Error('Invalid keys list'));
                return;
            }

            done(null, result[0], result[1]);
        });
};

/**
 * Получение собственно сообщений
 * @param {Number} nextCursor
 * @param {Array} keysNames
 * @param {Function} done
 * @private
 */
ErrorsMessagesReportModel.prototype._getErrorsMessages = function (nextCursor, keysNames, done) {

    this._redisClient.mget(keysNames, function (error, result) {
        if (error) {
            done(error);
            return;
        }

        this._report.totalErrors += result.length;

        this._iterationCallback(result);

        done(null, nextCursor, keysNames);

    }.bind(this));
};

/**
 * Удаление сообщений
 * @param {Number} nextCursor
 * @param {Array} keysNames
 * @param {Function} done
 * @private
 */
ErrorsMessagesReportModel.prototype._removeAllErrorsMessages = function (nextCursor, keysNames, done) {

    this._redisClient.del(keysNames, function (error, result) {
        if (error) {
            done(error);
            return;
        }

        this._report.deletedErrors += result;

        done(null, nextCursor);

    }.bind(this));
};

module.exports = ErrorsMessagesReportModel;
