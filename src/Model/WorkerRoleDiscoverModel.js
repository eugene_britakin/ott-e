'use strict';

/**
 * Модель сохранения/выбор состояния активного генератора
 *
 * @param {Object} redisClient
 * @param {Object} logger
 * @constructor
 */
var WorkerRoleDiscoverModel = function (redisClient, logger) {

    this._logger = logger;

    this._redisClient = redisClient;

    this._keyName = 'generator_worker_name';
};

WorkerRoleDiscoverModel.prototype.tryChooseGenerator = function (workerName, done) {
    this._redisClient
        .setnx(this._keyName, workerName, function (error, result) {
            if (error) {
                done(error);
                return;
            }

            if (typeof result === 'number' && result === 1) {
                done(null, true);
                return;
            }

            done(null, false);

        }.bind(this));
};

WorkerRoleDiscoverModel.prototype.prolongChoosenGenerator = function (workerName, expire, done) {
    this._redisClient.multi()
        .set(this._keyName, workerName)
        .expire(this._keyName, expire)
        .exec(
            function (error, result) {
                if (error) {
                    done(error);
                    return;
                }

                done(null, result);

            }.bind(this)
        );
};

module.exports = WorkerRoleDiscoverModel;
