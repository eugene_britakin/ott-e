'use strict';

var MessagesHandlerWorker = require('./Worker/MessagesHandlerWorker');
var MessagesGeneratorWorker = require('./Worker/MessagesGeneratorWorker');

/**
 * Менеджер воркеров, занимается переключением роли основного воркера (генератор/обработчик)
 *
 * @param {MessagesModel} messagesModel
 * @param {Object} logger
 * @constructor
 */
var WorkersManager = function (messagesModel, logger) {

    /**
     * Модель для работы с сообщениями
     * @type {MessagesModel}
     * @private
     */
    this._messagesModel = messagesModel;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;

    /**
     * Воркер генератор сообщений
     * @type {MessagesGeneratorWorker}
     * @private
     */
    this._messagesGeneratorWorker = null;

    /**
     * Воркер обработчик сообщений
     * @type {MessagesHandlerWorker}
     * @private
     */
    this._messagesHandlerWorker = null;
};

/**
 * Выбор роли воркера как генератор сообщений
 */
WorkersManager.prototype.setGeneratorRole = function () {
    this._logger.debug('WorkersManager enableMessagesGeneratorWorker');

    if (this._messagesHandlerWorker) {
        this._getMessagesHandlerWorker().disable();
    }

    this._getMessagesGeneratorWorker().enable();
};

/**
 * Выбор роли воркера как обработчик сообщений
 */
WorkersManager.prototype.setHandlerRole = function () {
    this._logger.debug('WorkersManager enableMessagesHandlerWorker');

    if (this._messagesGeneratorWorker) {
        this._getMessagesGeneratorWorker().disable();
    }

    this._getMessagesHandlerWorker().enable();
};

/**
 * Возвращает инстанс воркера обработчика, функция для ленивой инициализации
 * @return {MessagesHandlerWorker}
 * @private
 */
WorkersManager.prototype._getMessagesHandlerWorker = function () {
    if (this._messagesHandlerWorker) {
        return this._messagesHandlerWorker;
    }

    this._messagesHandlerWorker = new MessagesHandlerWorker(this._messagesModel, this._logger);
    return this._messagesHandlerWorker;
};

/**
 * Возвращает инстанс воркера генератора, функция для ленивой инициализации
 * т.к воркер и вовсе может не стать генераторм
 * @returns {MessagesGeneratorWorker}
 * @private
 */
WorkersManager.prototype._getMessagesGeneratorWorker = function () {
    if (this._messagesGeneratorWorker) {
        return this._messagesGeneratorWorker;
    }

    this._messagesGeneratorWorker = new MessagesGeneratorWorker(this._messagesModel, this._logger);
    return this._messagesGeneratorWorker;
};

module.exports = WorkersManager;
