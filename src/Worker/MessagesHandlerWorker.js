'use strict';

/**
 * Воркер обработки сообщений
 *
 * @param {MessagesModel} messagesModel
 * @param {Object} logger
 * @constructor
 */
var MessagesHandlerWorker = function (messagesModel, logger) {

    /**
     * Модель сообщений
     * @type {MessagesModel}
     * @private
     */
    this._messagesModel = messagesModel;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;

    /**
     * Флаг, что воркер активен
     * @type {Boolean}
     * @private
     */
    this._enabled = false;
};

/**
 * Включение логики работы воркера
 */
MessagesHandlerWorker.prototype.enable = function () {
    this._enabled = true;
    this._initSubscriber();
};

/**
 * Выключение воркера
 */
MessagesHandlerWorker.prototype.disable = function () {
    this._enabled = false;
    this._messagesModel.unsubscribe();
};

/**
 * Инициализация подписки на новые сообщения, при выкл воркера подписка удаляется
 * @private
 */
MessagesHandlerWorker.prototype._initSubscriber = function () {
    this._logger.debug('Init MessagesHandlerWorker');

    this._messagesModel.subscribe(function (error, name, msg) {

        if (error) {
            this._logger.error(error);
            return;
        }

        if (!this._enabled) {
            this._logger.warn('MessagesHandlerWorker skip message', name, msg);
            return;
        }

        this._eventHandler(msg, function (error, msg) {
            // если произошла ошибка обработки, сохраняем как сообщение с ошибкой
            if (error) {
                this._saveErrorMessage(name, msg);
                return;
            }
            // или успех ;)
            this._saveHandlendMessage(name, msg);

        }.bind(this));

    }.bind(this));
};

/**
 * Обработка события согласно ТЗ
 * @param {String} msg
 * @param {Function} callback
 * @private
 */
MessagesHandlerWorker.prototype._eventHandler = function (msg, callback) {

    function onComplete() {
        var error = Math.random() > 0.85;
        callback(error, msg);
    }

    // processing takes time...
    setTimeout(onComplete, Math.floor(Math.random() * 1000));
};

/**
 * Сохранение успешно обработанного сообщения
 * @param {String} name имя сообщения, для сохранения ключа
 * @param {String|Number} msg тело сообщения
 * @private
 */
MessagesHandlerWorker.prototype._saveHandlendMessage = function (name, msg) {
    this._messagesModel.saveHandlendMessage(name, msg, function (error) {
        if (error) {
            this._logger.error(error);
            return;
        }

        this._logger.debug('done:' + name, msg);
    }.bind(this));

};

/**
 * Сохранение сообщения обработанного с ошибкой
 * @param {String} name
 * @param {String|Number} msg
 * @private
 */
MessagesHandlerWorker.prototype._saveErrorMessage = function (name, msg) {
    this._messagesModel.saveErrorMessage(name, msg, function (error) {
        if (error) {
            this._logger.error(error);
            return;
        }

        this._logger.warn('error:' + name, msg);
    }.bind(this));
};

module.exports = MessagesHandlerWorker;
