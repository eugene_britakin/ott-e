'use strict';
var redis = require('redis');

/**
 * Воркер генератор сообщений, создает и рассылает сообщения
 *
 * @param {MessagesModel} messagesModel
 * @param {Object} logger
 * @constructor
 */
var MessagesGeneratorWorker = function (messagesModel, logger) {

    /**
     * Модель для работы с сообщениями
     * @type {MessagesModel}
     * @private
     */
    this._messagesModel = messagesModel;

    /**
     * Логгер приложения
     * @type {Object}
     * @private
     */
    this._logger = logger;

    /**
     * Флаг, что воркер активен
     * @type {Boolean}
     * @private
     */
    this._enabled = false;

    /**
     * Время тайм-аута генерации сообщения, ms
     * @type {Number}
     * @private
     */
    this._timeoutTime = 500;

    /**
     * Служебное поле, используется для очистки тайм-аута, генерации сообщений
     * @type {Function}
     * @private
     */
    this._timeoutSendMessage = null;
};

/**
 * Включение работы воркера
 */
MessagesGeneratorWorker.prototype.enable = function () {
    this._enabled = true;
    this._logger.info('Init MessagesGeneratorWorker');
    this._publishMessage();
};

/**
 * Отключение работы воркера
 */
MessagesGeneratorWorker.prototype.disable = function () {
    this._enabled = false;
    if (this._timeoutSendMessage) {
        clearTimeout(this._timeoutSendMessage);
    }
};

/**
 * Возвращает тело нового сообщения, реализация взята из ТЗ
 *
 * @return {Number}
 * @private
 */
MessagesGeneratorWorker.prototype._getMessage = function () {
    this.cnt = this.cnt || 0;
    return this.cnt++;
};

/**
 * Выполняет публикацию сообщения
 * @private
 */
MessagesGeneratorWorker.prototype._publishMessage = function () {
    var newMsg = this._getMessage();

    this._messagesModel.publishNewMessage(newMsg, function (error, result) {
        if (error) {
            this._logger.error(error);
            return;
        }

        this._logger.debug('Generator published new message', newMsg, result);
        this._delayPublishMessage();

    }.bind(this));
};

/**
 * Выполняет отложенную публикацию сообщения
 * @private
 */
MessagesGeneratorWorker.prototype._delayPublishMessage = function () {
    if (this._timeoutSendMessage) {
        clearTimeout(this._timeoutSendMessage);
    }

    this._timeoutSendMessage = setTimeout(
        this._publishMessage.bind(this),
        this._timeoutTime
    );
};

module.exports = MessagesGeneratorWorker;
