process.stdin.resume();
var fork = require('child_process').fork;
var os = require('os');

//var config = require('./config')();
//var redis = require('redis');
//redis.createClient(config.redis).flushdb();

var cpuCount = os.cpus().length;
var workers = cpuCount + 1;
for (var i = 1; i <= workers; i++) {
    var workerProcess = fork('./worker.js');
    workerProcess.on('close', function (code) {
        console.log('Child process exited with code ' + code);
    });
}
