/**
 * Created by Eugene on 27.07.2016.
 *
 * @see demo https://jsfiddle.net/s7Luokdu/12/
 */

var Plainer = function (data) {

    /**
     * Входящие данные
     * @type {Array}
     */
    this._data = data;

    /**
     * Текущая позиция по оси X
     * @type {Number}
     */
    this._currentPositionX = 0;

    /**
     * Текущая позиция по оси Y
     * @type {Number}
     */
    this._currentPositionY = 0;

    /**
     * Текущее название действия перемещения
     * @type {String}
     */
    this._move = 'left';

    /**
     * Итерации между дейсвтиями
     * @type {Number}
     */
    this._iterations = 1;

    /**
     * Лимит элементов для забора по горизонтали
     * @type {Number}
     */
    this._horizontalLimit = 1;

    /**
     * Лимит элементов для забора по вертикали
     * @type {Number}
     */
    this._verticalLimit = 1;
};

/**
 * Возвращает плоский список значений по спирали от центра
 * @return {Array}
 */
Plainer.prototype.getPlainList = function () {
    var elsInRow = this._data.length;
    var totalEls = elsInRow * elsInRow;

    this._currentPositionX = Math.floor(elsInRow / 2);
    this._currentPositionY = this._currentPositionX;

    var result = [];
    for (var i = 0; i < totalEls; i++) {

        if (!this._data[this._currentPositionX]) {
            console.error(';((( xxx', this._currentPositionX);
            continue;
        }

        if (!this._data[this._currentPositionX][this._currentPositionY]) {
            console.error(';((( xxx', this._currentPositionX, 'yyyy', this._currentPositionY);
            continue;
        }

        result.push(
            this._data[this._currentPositionX][this._currentPositionY]
        );

        if (this._move === 'left') {
            this._proceedMoveLeft();
            continue;
        }

        if (this._move === 'down') {
            this._proceedMoveDown();
            continue;
        }

        if (this._move === 'right') {
            this._proceedMoveRight();
            continue;
        }

        if (this._move === 'up') {
            this._proceedMoveUp();
            continue;
        }
    }

    return result;
};

Plainer.prototype._proceedMoveLeft = function () {
    this._currentPositionY--;
    this._iterations--;

    if (!this._iterations) {
        this._iterations = this._verticalLimit;
        this._horizontalLimit++;
        this._move = 'down';
    }
};

Plainer.prototype._proceedMoveDown = function () {
    this._currentPositionX++;
    this._iterations--;

    if (!this._iterations) {
        this._iterations = this._horizontalLimit;
        this._verticalLimit++;
        this._move = 'right';
    }
};

Plainer.prototype._proceedMoveRight = function () {
    this._currentPositionY++;
    this._iterations--;

    if (!this._iterations) {
        this._iterations = this._verticalLimit;
        this._horizontalLimit++;
        this._move = 'up';
    }
};

Plainer.prototype._proceedMoveUp = function () {
    this._currentPositionX--;
    this._iterations--;

    if (!this._iterations) {
        this._iterations = this._horizontalLimit;
        this._verticalLimit++;
        this._move = 'left';
    }
};


/**
 * Генерирует матрицу
 * @param {Number} n
 * @return {Array}
 */
function generateMatrix(n) {
    var els = 2 * n - 1;
    var data = [];
    var i = 1;

    for (var row = 0; row < els; row++) {
        if (!data[row]) {
            data[row] = [];
        }

        for (var cell = 0; cell < els; cell++) {
            data[row].push(i);
            i++;
        }
    }

    return data;
}

/////////////////////////////////////////////////////////

var args = [2, 3, 5];
for (var i = 0; i < args.length; i++) {
    var data = generateMatrix(args[i]);

    var els = 2 * args[i] - 1;

    console.log("*************** " + els + " ***************");

    var pl = new Plainer(data);
    var result = pl.getPlainList();

    console.log(data);

    console.log(result);

    console.log("*************** /" + els + " ***************");
}
