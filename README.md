# README #

### 1. Генератор и обработчики событий ###

```bash
$ npm i

// запуск одного воркера
$ node worker.js

// запуск пачки воркеров
$ node pm.js
```


### 2. Вывод значений матрицы от центра по спирали ###

```bash
$ npm i

$ node second.js

```

- [Demo](https://jsfiddle.net/n67nv93c/)